**Niveau et Durée** : 1 séance.

**Dans les programmes du niveau visé** : Algorithmique et programmation
- Variables et instruction élémentaires: écrire une formule permettant un calcul combinant des variables ;
- Programmer une instruction conditionnelle ;
- Notion de fonction : Programmer des fonctions simples ayant un petit nombre d’arguments.

**TP** :
Dessiner un rectagle (avec l'activité Turtle).
Dessiner ses diamètres.
