**Thématique** : Réseaux et internet . 

**Notions liées** : Topologie réseau, typologie réseau .

**Résumé de l’activité** : deux activités de base sur le module réseau et internet pour les élèves en difficultés .

**Objectifs** : Découverte des bases de Réseau , Les typologie réseau et les topologies réseau  .

**Auteur**: Oumaima AL MOBARAKI

**Durée de l’activité** : 1h

**Forme de participation**: individuelle ou en binôme, en autonomie, collective

**Matériel nécessaire** : Papier, stylo 

**Préparation** : Aucune

**Autres références**: Fiche élève .

