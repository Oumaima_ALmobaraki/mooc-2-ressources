**Objectifs** : Maîtriser les principaux concepts de réseau 

**Pré-requis à cette activité** : Informatique , Traitement , Information 

**Durée de l'activité**: une heure 

**Exercices cibles** : Exercice n°1 , Exercice n°2 

**Description du déroulement de l'activité** : 

-L'activité de l'enseignant : Présenter le travail à faire, intervenir pour aider les apprenants (orienter, guider, faciliter).

-Lactivité de l'élève : Faire les activité Et créer une mindmapping sur les notion de base d'un réseau .

**Anticipation des difficultés des élèves** : 

- Rappeler les notion de base qu’on a vue .

- difficulté de distinction entre les typologie et topologie .

