**Objectifs** : Découvrir la notion de fonction algorithmique et/ou la notion de test pour calculer des factures dans différentes situations.

**Pré-requis à cette activité** : La notion d'algorithmique, les algorithmes, la notion de langage de programmation.

**Durée de l'activité** : 2h.

**Situation poblème** : Calculer à l’aide d’un script en Python le montant d’une facture en fonction du nombre de pains et de croissants achetés, avec différentes modalités de tarification. 

**Description du déroulement de l'activité** : Les élèves prennent connaissance de la consigne et élaborent à leur rythme les scripts demandés avant de les tester. Une synthèse sur la notion de fonction algorithmique et/ou la notion de test est rédigée collectivement en fin de séance. Des exercices de mise en œuvre sont proposés en fin de document pour les élèves les plus performants.

**Anticipation des difficultés des élèves** : La réduction en pourcentage. Les problèmes de syntaxe, de saisie, d’indentation… seront nombreux, d’où la nécessité de différencier à l’aide d’exercices supplémentaires en fin de fiche.
